using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPScontroller : MonoBehaviour
{
   public Camera cam;
   public float walkSpeed = 5f;       //Walk speed
   public float hRotationSpeed = 100f;  //Player rotates along y axis
   public float vRotationSpeed = 80f;   // cam rotates along x axis
    
    // Use this for initialization
    void Start()
    {
    // Hide and look mouse cursor
       Cursor.visible = false;
       Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

      } 
      //Update is called once per frame 
    void Update()
    {
        movement();
       
    }

    private void movement(){

        // Codigo de movimiento
        float hMovement = Input.GetAxisRaw("Horizontal") ;
       float vMovement = Input.GetAxisRaw("Vertical") ;

       Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
       transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));

       //Rotation
       float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
       float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

       transform.Rotate(0f,hPlayerRotation,0f);
       cam.transform.Rotate(-vCamRotation,0f,0f);
        }
}
